import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
defineLocale('es', esLocale);
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SexoDominanteComponent } from './components/dashboard/sexo-dominante/sexo-dominante.component';
import { UsuariosComponent } from './components/dashboard/usuarios/usuarios.component';
import { RangoEdadComponent } from './components/dashboard/rango-edad/rango-edad.component';
import { TonoPielDominanteComponent } from './components/dashboard/tono-piel-dominante/tono-piel-dominante.component';
import { ConsultasComponent } from './components/dashboard/consultas/consultas.component';
import { ConsultasHistoricasComponent } from './components/dashboard/consultas-historicas/consultas-historicas.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ConsultasPromedioUsuarioComponent } from './components/dashboard/consultas-promedio-usuario/consultas-promedio-usuario.component';
import { ConsultasMesComponent } from './components/dashboard/consultas-mes/consultas-mes.component';
import { TiempoPromedioConsultasComponent } from './components/dashboard/tiempo-promedio-consultas/tiempo-promedio-consultas.component';
import { TiempoPromedioSexoComponent } from './components/dashboard/tiempo-promedio-sexo/tiempo-promedio-sexo.component';
import { TiempoPromedioConsultasEstadoComponent } from './components/dashboard/tiempo-promedio-consultas-estado/tiempo-promedio-consultas-estado.component';
import { UsuariosSinConsultasComponent } from './components/dashboard/usuarios-sin-consultas/usuarios-sin-consultas.component';
import { TonoPielPromedioComponent } from './components/dashboard/tono-piel-promedio/tono-piel-promedio.component';
import { SeguimientosTotalesComponent } from './components/dashboard/seguimientos-totales/seguimientos-totales.component';
import { SeguimientosComponent } from './components/dashboard/seguimientos/seguimientos.component';
import { CambioPromedioColorComponent } from './components/dashboard/cambio-promedio-color/cambio-promedio-color.component';
import { CambioPromedioTamanioComponent } from './components/dashboard/cambio-promedio-tamanio/cambio-promedio-tamanio.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SexoDominanteComponent,
    UsuariosComponent,
    RangoEdadComponent,
    TonoPielDominanteComponent,
    ConsultasComponent,
    ConsultasHistoricasComponent,
    ConsultasPromedioUsuarioComponent,
    ConsultasMesComponent,
    TiempoPromedioConsultasComponent,
    TiempoPromedioSexoComponent,
    TiempoPromedioConsultasEstadoComponent,
    UsuariosSinConsultasComponent,
    TonoPielPromedioComponent,
    SeguimientosTotalesComponent,
    SeguimientosComponent,
    CambioPromedioColorComponent,
    CambioPromedioTamanioComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxChartsModule,
    TooltipModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
