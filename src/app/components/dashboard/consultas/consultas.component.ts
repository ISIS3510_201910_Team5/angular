import { Component, OnInit, Input } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.scss']
})
export class ConsultasComponent implements OnInit {
  public colorTheme = 'theme-dark-blue';

  public locale = 'es';

  public bsConfig: Partial<BsDatepickerConfig>;

  public texto = '';

  public error = false;

  public numeroConsultas = -1;

  public fechaMaxima = new Date();

  public fechaMinima = new Date('01/01/2019');

  public rangoFechas: Date[] = [this.fechaMinima, this.fechaMaxima];

  public loading = true;

  public view = [400, 300];

  public colorScheme = {
    domain: ['#C0C0C0', '#ff6700', '#43A047', '#B00020']
  };

  public consultas = [
    {
      nombre: 'numeroDeConsultasSinAnalizar',
      name: 'No analizada',
      value: -1
    },
    {
      nombre: 'numeroDeConsultasPosiblementePeligrosas',
      name: 'Potencialmente peligrosa',
      value: -1
    },
    {
      nombre: 'numeroDeConsultasNoPeligrosas',
      name: 'No peligrosa',
      value: -1
    },
    {
      nombre: 'numeroDeConsultasPeligrosas',
      name: 'Peligrosa',
      value: -1
    }
  ];

  constructor(
    private dashboardService: DashboardService,
    private localeService: BsLocaleService
  ) {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.localeService.use(this.locale);
  }

  ngOnInit() {
    this.realizarConsultas();
  }

  calcularFecha(opcion) {
    const mes = this.fechaMaxima.getMonth() + 1;
    const anio = this.fechaMaxima.getFullYear();
    if (opcion === 0) {
      this.rangoFechas = [this.fechaMaxima, this.fechaMaxima];
    } else if (opcion === 1) {
      this.rangoFechas = [new Date(mes + '/01/' + anio), this.fechaMaxima];
    } else if (opcion === 2) {
      this.rangoFechas = [new Date('01/01/' + anio), this.fechaMaxima];
    }
    this.buscarPorFecha();
  }

  buscarPorFecha() {
    this.numeroConsultas = -1;
    this.consultas[0].value = -1;
    this.consultas[1].value = -1;
    this.consultas[2].value = -1;
    this.consultas[3].value = -1;
    this.realizarConsultas();
  }

  realizarConsultas() {
    this.dashboardService
      .consultas(
        'numeroDeConsultas',
        this.rangoFechas[0].toLocaleDateString(),
        this.rangoFechas[1].toLocaleDateString()
      )
      .then((data: number) => {
        console.log(data);

        this.numeroConsultas = data;
      })
      .catch(err => {
        this.error = true;
      });
    this.realizarConsulta(0);
    this.realizarConsulta(1);
    this.realizarConsulta(2);
    this.realizarConsulta(3);
  }

  realizarConsulta(posicion) {
    this.dashboardService
      .consultas(
        this.consultas[posicion].nombre,
        this.rangoFechas[0].toLocaleDateString(),
        this.rangoFechas[1].toLocaleDateString()
      )
      .then((data: number) => {
        this.consultas[posicion].value = data;
      })
      .catch(err => {
        this.error = true;
      });
  }
}
