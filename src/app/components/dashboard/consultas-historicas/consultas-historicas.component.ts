import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-consultas-historicas',
  templateUrl: './consultas-historicas.component.html',
  styleUrls: ['./consultas-historicas.component.scss']
})
export class ConsultasHistoricasComponent implements OnInit {
  public loading = true;

  public consultas = -1;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    const fechaActual = new Date().toLocaleDateString();
    this.dashboardService
      .consultas('numeroDeConsultas', '01/01/2019', fechaActual)
      .then((data: number) => {
        this.consultas = data;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
