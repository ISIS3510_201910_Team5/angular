import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultasHistoricasComponent } from './consultas-historicas.component';

describe('ConsultasHistoricasComponent', () => {
  let component: ConsultasHistoricasComponent;
  let fixture: ComponentFixture<ConsultasHistoricasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultasHistoricasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultasHistoricasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
