import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-tono-piel-dominante',
  templateUrl: './tono-piel-dominante.component.html',
  styleUrls: ['./tono-piel-dominante.component.scss']
})
export class TonoPielDominanteComponent implements OnInit {
  public loading = true;

  public tonoDePielPromedio = '';

  public tonoDePielDesviacion = '';

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService
      .tonoDePielPredominante()
      .then((data: any) => {
        this.tonoDePielPromedio = data.average.toFixed(2);
        this.tonoDePielDesviacion = data.standarDeviation.toFixed(2);
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
