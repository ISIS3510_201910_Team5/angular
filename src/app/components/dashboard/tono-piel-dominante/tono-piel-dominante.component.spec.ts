import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TonoPielDominanteComponent } from './tono-piel-dominante.component';

describe('TonoPielDominanteComponent', () => {
  let component: TonoPielDominanteComponent;
  let fixture: ComponentFixture<TonoPielDominanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TonoPielDominanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TonoPielDominanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
