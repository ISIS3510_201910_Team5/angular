import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-tiempo-promedio-sexo',
  templateUrl: './tiempo-promedio-sexo.component.html',
  styleUrls: ['./tiempo-promedio-sexo.component.scss']
})
export class TiempoPromedioSexoComponent implements OnInit {
  public loading = true;

  public femenino = '-1';

  public masculino = '-1';

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    const fechaActual = new Date().toLocaleDateString();
    this.dashboardService
      .tiempoPromedioPorGenero('01/01/2019', fechaActual)
      .then((data: any) => {
        console.log(data);

        this.masculino = (
          data.promedioDeTiempoPorConsultasHombres / 1000
        ).toFixed(2);
        this.femenino = (
          data.promedioDeTiempoPorConsultasMujeres / 1000
        ).toFixed(2);
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
