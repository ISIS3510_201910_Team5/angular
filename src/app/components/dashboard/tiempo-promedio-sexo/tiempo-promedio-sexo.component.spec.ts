import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiempoPromedioSexoComponent } from './tiempo-promedio-sexo.component';

describe('TiempoPromedioSexoComponent', () => {
  let component: TiempoPromedioSexoComponent;
  let fixture: ComponentFixture<TiempoPromedioSexoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiempoPromedioSexoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiempoPromedioSexoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
