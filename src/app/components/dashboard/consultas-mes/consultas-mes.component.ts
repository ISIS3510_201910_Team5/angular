import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-consultas-mes',
  templateUrl: './consultas-mes.component.html',
  styleUrls: ['./consultas-mes.component.scss']
})
export class ConsultasMesComponent implements OnInit {
  public colorTheme = 'theme-dark-blue';

  public locale = 'es';

  public bsConfig: Partial<BsDatepickerConfig>;

  public texto = 'peligrosas';

  public error = false;

  public numeroConsultas = 0;

  public loading = true;

  public opcion = 0;

  public anio;

  public consultas = [
    {
      mes: '01',
      name: 'Enero',
      value: 0,
      listo: false
    },
    {
      mes: '02',
      name: 'Febrero',
      value: 0,
      listo: false
    },
    {
      mes: '03',
      name: 'Marzo',
      value: 0,
      listo: false
    },
    {
      mes: '04',
      name: 'Abril',
      value: 0,
      listo: false
    },
    {
      mes: '05',
      name: 'Mayo',
      value: 0,
      listo: false
    },
    {
      mes: '06',
      name: 'Junio',
      value: 0,
      listo: false
    },
    {
      mes: '06',
      name: 'Julio',
      value: 0,
      listo: false
    },
    {
      mes: '08',
      name: 'Agosto',
      value: 0,
      listo: false
    },
    {
      mes: '09',
      name: 'Septiembre',
      value: 1,
      listo: false
    },
    {
      mes: '10',
      name: 'Octubre',
      value: 1,
      listo: false
    },
    {
      mes: '11',
      name: 'Noviembre',
      value: 1,
      listo: false
    },
    {
      mes: '12',
      name: 'Diciembre',
      value: 1,
      listo: false
    }
  ];

  constructor(
    private dashboardService: DashboardService,
    private localeService: BsLocaleService
  ) {
    this.bsConfig = Object.assign({}, { containerClass: this.colorTheme });
    this.localeService.use(this.locale);
    this.anio = new Date().getFullYear();
  }

  ngOnInit() {
    this.realizarConsultas('numeroDeConsultasPeligrosas');
  }

  cambiarOpcion(opcion) {
    this.opcion = opcion;
    let nombre = 'numeroDeConsultas';

    if (opcion === 0) {
      nombre += 'Peligrosas';
      this.texto = 'peligrosas';
    } else if (opcion === 1) {
      nombre += 'PosiblementePeligrosas';
      this.texto = 'potencialmente peligrosas';
    } else if (opcion === 2) {
      nombre += 'NoPeligrosas';
      this.texto = 'no peligrosas';
    } else {
      nombre += 'SinAnalizar';
      this.texto = 'no analizadas';
    }
    this.reiniciarConsultas();
    this.realizarConsultas(nombre);
  }

  realizarConsultas(nombre) {
    let consulta;
    let fechaInicio = '';
    let fechaFin = '';
    for (let index = 0; index < this.consultas.length; index++) {
      consulta = this.consultas[index];
      fechaInicio = '01/' + consulta.mes + '/' + this.anio;
      fechaFin = '31/' + consulta.mes + '/' + this.anio;
      this.realizarConsulta(nombre, fechaInicio, fechaFin, index);
    }
  }

  reiniciarConsultas() {
    this.consultas.map(consulta => {
      consulta.listo = false;
    });
  }

  realizarConsulta(nombre, fechaInicio, fechaFin, posicion) {
    this.dashboardService
      .consultas(nombre, fechaInicio, fechaFin)
      .then((data: number) => {
        console.log(data);

        this.consultas[posicion].value = data;
        this.consultas[posicion].listo = true;
      })
      .catch(err => {
        this.error = true;
      });
  }
}
