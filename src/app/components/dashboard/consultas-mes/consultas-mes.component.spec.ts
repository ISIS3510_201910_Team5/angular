import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultasMesComponent } from './consultas-mes.component';

describe('ConsultasMesComponent', () => {
  let component: ConsultasMesComponent;
  let fixture: ComponentFixture<ConsultasMesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultasMesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultasMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
