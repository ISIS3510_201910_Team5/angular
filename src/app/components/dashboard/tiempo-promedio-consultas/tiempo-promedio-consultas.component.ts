import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-tiempo-promedio-consultas',
  templateUrl: './tiempo-promedio-consultas.component.html',
  styleUrls: ['./tiempo-promedio-consultas.component.scss']
})
export class TiempoPromedioConsultasComponent implements OnInit {
  public loading = true;

  public consultas = -1;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    const fechaActual = new Date().toLocaleDateString();
    this.dashboardService
      .tiempoPromedioConsultas(
        'tiempoDeAnalisisPromedio',
        '01/01/2019',
        fechaActual
      )
      .then((data: number) => {
        this.consultas = data;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
