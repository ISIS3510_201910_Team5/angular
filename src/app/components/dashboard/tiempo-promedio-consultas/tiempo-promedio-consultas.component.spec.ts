import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiempoPromedioConsultasComponent } from './tiempo-promedio-consultas.component';

describe('TiempoPromedioConsultasComponent', () => {
  let component: TiempoPromedioConsultasComponent;
  let fixture: ComponentFixture<TiempoPromedioConsultasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiempoPromedioConsultasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiempoPromedioConsultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
