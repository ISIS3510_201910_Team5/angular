import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RangoEdadComponent } from './rango-edad.component';

describe('RangoEdadComponent', () => {
  let component: RangoEdadComponent;
  let fixture: ComponentFixture<RangoEdadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RangoEdadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangoEdadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
