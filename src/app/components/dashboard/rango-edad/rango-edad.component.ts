import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-rango-edad',
  templateUrl: './rango-edad.component.html',
  styleUrls: ['./rango-edad.component.scss']
})
export class RangoEdadComponent implements OnInit {
  public loading = true;

  public mayor = -1;

  public menor = -1;

  public usuarios = -1;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService
      .rangoDeEdad()
      .then((data: any) => {
        this.mayor = data.limSup;
        this.menor = data.limInf;
        this.usuarios = data.numeroUsuarios;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
