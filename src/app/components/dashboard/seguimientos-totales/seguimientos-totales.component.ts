import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-seguimientos-totales',
  templateUrl: './seguimientos-totales.component.html',
  styleUrls: ['./seguimientos-totales.component.scss']
})
export class SeguimientosTotalesComponent implements OnInit {
  public loading = true;

  public seguimientos = -1;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    const fechaActual = new Date().toLocaleDateString();
    this.dashboardService
      .seguimientos(
        'numeroDeSiguimientosConMasDeUnaConsulta',
        '01/01/2019',
        fechaActual
      )
      .then((data: number) => {
        this.seguimientos = data;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
