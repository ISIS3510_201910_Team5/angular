import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguimientosTotalesComponent } from './seguimientos-totales.component';

describe('SeguimientosTotalesComponent', () => {
  let component: SeguimientosTotalesComponent;
  let fixture: ComponentFixture<SeguimientosTotalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguimientosTotalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguimientosTotalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
