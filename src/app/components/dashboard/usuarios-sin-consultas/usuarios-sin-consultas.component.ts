import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-usuarios-sin-consultas',
  templateUrl: './usuarios-sin-consultas.component.html',
  styleUrls: ['./usuarios-sin-consultas.component.scss']
})
export class UsuariosSinConsultasComponent implements OnInit {
  public loading = true;

  public usuarios = -1;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService
      .usuariosSinConsultas()
      .then((data: number) => {
        this.usuarios = data;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
