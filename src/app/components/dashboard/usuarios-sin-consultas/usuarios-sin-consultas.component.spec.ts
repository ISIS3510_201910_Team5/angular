import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariosSinConsultasComponent } from './usuarios-sin-consultas.component';

describe('UsuariosSinConsultasComponent', () => {
  let component: UsuariosSinConsultasComponent;
  let fixture: ComponentFixture<UsuariosSinConsultasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuariosSinConsultasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariosSinConsultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
