import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  public loading = true;

  public usuarios = -1;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService
      .usuarios()
      .then((data: number) => {
        this.usuarios = data;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
