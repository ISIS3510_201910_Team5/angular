import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambioPromedioColorComponent } from './cambio-promedio-color.component';

describe('CambioPromedioColorComponent', () => {
  let component: CambioPromedioColorComponent;
  let fixture: ComponentFixture<CambioPromedioColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambioPromedioColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambioPromedioColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
