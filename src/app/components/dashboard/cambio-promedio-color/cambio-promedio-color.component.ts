import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-cambio-promedio-color',
  templateUrl: './cambio-promedio-color.component.html',
  styleUrls: ['./cambio-promedio-color.component.scss']
})
export class CambioPromedioColorComponent implements OnInit {
  public error = false;

  public loading = true;

  public opcion = 0;

  public colorScheme = {
    domain: ['#B00020', '#ff6700', '#43A047']
  };

  public cambios = [
    {
      mes: '01',
      name: 'Peligrosas',
      peticion: 'cambioDeColorPromedioEnSeguimientosPeligrosos',
      value: 0,
      listo: false
    },
    {
      mes: '02',
      name: 'Potencialmente peligrosas',
      peticion: 'cambioDeColorPromedioEnSeguimientosPosiblementePeligrosos',
      value: 0,
      listo: false
    },
    {
      mes: '03',
      name: 'No peligrosas',
      peticion: 'cambioDeColorPromedioEnSeguimientosNoPeligrosos',
      value: 0,
      listo: false
    }
  ];

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.obtenerPromedios();
  }

  obtenerPromedios() {
    let consulta;
    const fechaInicio = '01/01/2019';
    const fechaFin = new Date().toLocaleDateString();
    for (let index = 0; index < this.cambios.length; index++) {
      consulta = this.cambios[index];
      this.obtenerPromedio(consulta.peticion, fechaInicio, fechaFin, index);
    }
  }

  obtenerPromedio(nombre, fechaInicio, fechaFin, posicion) {
    this.dashboardService
      .cambioColorPromedio(nombre, fechaInicio, fechaFin)
      .then((data: number) => {
        console.log(data);
        this.cambios[posicion].value = data;
        this.cambios[posicion].listo = true;
      })
      .catch(err => {
        this.error = true;
      });
  }
}
