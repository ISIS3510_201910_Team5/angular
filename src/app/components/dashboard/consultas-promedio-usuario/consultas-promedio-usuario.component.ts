import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-consultas-promedio-usuario',
  templateUrl: './consultas-promedio-usuario.component.html',
  styleUrls: ['./consultas-promedio-usuario.component.scss']
})
export class ConsultasPromedioUsuarioComponent implements OnInit {
  public loading = true;

  public consultas = '-1';

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    const fechaActual = new Date().toLocaleDateString();
    this.dashboardService
      .consultasPromedioPorUsuario('01/01/2019', fechaActual)
      .then((data: number) => {
        this.consultas = data.toFixed(2);
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
