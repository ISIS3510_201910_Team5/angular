import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultasPromedioUsuarioComponent } from './consultas-promedio-usuario.component';

describe('ConsultasPromedioUsuarioComponent', () => {
  let component: ConsultasPromedioUsuarioComponent;
  let fixture: ComponentFixture<ConsultasPromedioUsuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultasPromedioUsuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultasPromedioUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
