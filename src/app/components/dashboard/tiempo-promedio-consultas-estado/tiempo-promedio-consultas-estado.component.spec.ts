import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiempoPromedioConsultasEstadoComponent } from './tiempo-promedio-consultas-estado.component';

describe('TiempoPromedioConsultasEstadoComponent', () => {
  let component: TiempoPromedioConsultasEstadoComponent;
  let fixture: ComponentFixture<TiempoPromedioConsultasEstadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiempoPromedioConsultasEstadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiempoPromedioConsultasEstadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
