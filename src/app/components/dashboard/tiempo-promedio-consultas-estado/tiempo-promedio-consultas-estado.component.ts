import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-tiempo-promedio-consultas-estado',
  templateUrl: './tiempo-promedio-consultas-estado.component.html',
  styleUrls: ['./tiempo-promedio-consultas-estado.component.scss']
})
export class TiempoPromedioConsultasEstadoComponent implements OnInit {
  public error = false;

  public numeroConsultas = 0;

  public loading = true;

  public opcion = 0;

  public colorScheme = {
    domain: ['#B00020', '#ff6700', '#43A047']
  };

  public consultas = [
    {
      mes: '01',
      name: 'Peligrosas',
      peticion: 'tiempoDeAnalisisPromedioConsultasPeligrosas',
      value: 0,
      listo: false
    },
    {
      mes: '02',
      name: 'Potencialmente peligrosas',
      peticion: 'tiempoDeAnalisisPromedioConsultasPosiblementePeligrosas',
      value: 0,
      listo: false
    },
    {
      mes: '03',
      name: 'No peligrosas',
      peticion: 'tiempoDeAnalisisPromedioConsultasNoPeligrosas',
      value: 0,
      listo: false
    }
  ];

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.realizarConsultas();
  }

  realizarConsultas() {
    let consulta;
    const fechaInicio = '01/01/2019';
    const fechaFin = new Date().toLocaleDateString();
    for (let index = 0; index < this.consultas.length; index++) {
      consulta = this.consultas[index];
      this.realizarConsulta(consulta.peticion, fechaInicio, fechaFin, index);
    }
  }

  realizarConsulta(nombre, fechaInicio, fechaFin, posicion) {
    this.dashboardService
      .tiempoPromedioConsultas(nombre, fechaInicio, fechaFin)
      .then((data: number) => {
        console.log(data);
        this.consultas[posicion].value = data;
        this.consultas[posicion].listo = true;
      })
      .catch(err => {
        this.error = true;
      });
  }
}
