import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-seguimientos',
  templateUrl: './seguimientos.component.html',
  styleUrls: ['./seguimientos.component.scss']
})
export class SeguimientosComponent implements OnInit {
  public texto = '';

  public error = false;

  public numeroSeguimientos = -1;

  public loading = true;

  public view = [400, 300];

  public fechaActual;

  public colorScheme = {
    domain: ['#ff6700', '#43A047', '#B00020']
  };

  public seguimientos = [
    {
      nombre: 'numeroDeSiguimientosPosiblementePeligrosos',
      name: 'Potencialmente peligrosa',
      value: -1
    },
    {
      nombre: 'numeroDeSiguimientosNoPeligrosos',
      name: 'No peligrosa',
      value: -1
    },
    {
      nombre: 'numeroDeSiguimientosPeligrosos',
      name: 'Peligrosa',
      value: -1
    }
  ];

  constructor(private dashboardService: DashboardService) {
    this.fechaActual = new Date().toLocaleDateString();
  }

  ngOnInit() {
    this.realizarSeguimientos();
  }

  buscarPorFecha() {
    this.numeroSeguimientos = -1;
    this.seguimientos[0].value = -1;
    this.seguimientos[1].value = -1;
    this.seguimientos[2].value = -1;
    this.realizarSeguimientos();
  }

  realizarSeguimientos() {
    this.dashboardService
      .seguimientos(
        'numeroDeSiguimientosConMasDeUnaConsulta',
        '01/01/2019',
        this.fechaActual
      )
      .then((data: number) => {
        console.log(data);

        this.numeroSeguimientos = data;
      })
      .catch(err => {
        this.error = true;
      });
    this.realizarSeguimiento(0);
    this.realizarSeguimiento(1);
    this.realizarSeguimiento(2);
  }

  realizarSeguimiento(posicion) {
    this.dashboardService
      .seguimientos(
        this.seguimientos[posicion].nombre,
        '01/01/2019',
        this.fechaActual
      )
      .then((data: number) => {
        console.log(data);

        this.seguimientos[posicion].value = data;
      })
      .catch(err => {
        this.error = true;
      });
  }
}
