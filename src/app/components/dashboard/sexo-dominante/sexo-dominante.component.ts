import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-sexo-dominante',
  templateUrl: './sexo-dominante.component.html',
  styleUrls: ['./sexo-dominante.component.scss']
})
export class SexoDominanteComponent implements OnInit {
  public loading = true;

  public femenino = -1;

  public masculino = -1;

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService
      .sexoPredominante()
      .then((data: any) => {
        this.masculino = data.masculino;
        this.femenino = data.femenino;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
