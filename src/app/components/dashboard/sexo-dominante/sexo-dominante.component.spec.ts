import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SexoDominanteComponent } from './sexo-dominante.component';

describe('SexoDominanteComponent', () => {
  let component: SexoDominanteComponent;
  let fixture: ComponentFixture<SexoDominanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SexoDominanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SexoDominanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
