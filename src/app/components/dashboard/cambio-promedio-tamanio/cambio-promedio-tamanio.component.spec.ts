import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CambioPromedioTamanioComponent } from './cambio-promedio-tamanio.component';

describe('CambioPromedioTamanioComponent', () => {
  let component: CambioPromedioTamanioComponent;
  let fixture: ComponentFixture<CambioPromedioTamanioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CambioPromedioTamanioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CambioPromedioTamanioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
