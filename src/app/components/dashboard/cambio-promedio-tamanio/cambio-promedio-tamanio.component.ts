import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-cambio-promedio-tamanio',
  templateUrl: './cambio-promedio-tamanio.component.html',
  styleUrls: ['./cambio-promedio-tamanio.component.scss']
})
export class CambioPromedioTamanioComponent implements OnInit {
  public error = false;

  public loading = true;

  public opcion = 0;

  public colorScheme = {
    domain: ['#B00020', '#ff6700', '#43A047']
  };

  public cambios = [
    {
      mes: '01',
      name: 'Peligrosas',
      peticion: 'averageSizeChangePeligrosas',
      value: 0,
      listo: false
    },
    {
      mes: '02',
      name: 'Potencialmente peligrosas',
      peticion: 'averageSizeChangePosiblementePeligrosas',
      value: 0,
      listo: false
    },
    {
      mes: '03',
      name: 'No peligrosas',
      peticion: 'averageSizeChangeNoPeligrosas',
      value: 0,
      listo: false
    }
  ];

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.obtenerPromedios();
  }

  obtenerPromedios() {
    let consulta;
    const fechaInicio = '01/01/2019';
    const fechaFin = new Date().toLocaleDateString();
    for (let index = 0; index < this.cambios.length; index++) {
      consulta = this.cambios[index];
      this.obtenerPromedio(consulta.peticion, fechaInicio, fechaFin, index);
    }
  }

  obtenerPromedio(nombre, fechaInicio, fechaFin, posicion) {
    this.dashboardService
      .cambioTamanioPromedio(nombre, fechaInicio, fechaFin)
      .then((data: number) => {
        console.log(data);
        this.cambios[posicion].value = data;
        this.cambios[posicion].listo = true;
      })
      .catch(err => {
        this.error = true;
      });
  }
}
