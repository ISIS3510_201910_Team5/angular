import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-tono-piel-promedio',
  templateUrl: './tono-piel-promedio.component.html',
  styleUrls: ['./tono-piel-promedio.component.scss']
})
export class TonoPielPromedioComponent implements OnInit {
  public loading = true;

  public tonoDePiel = '';

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService
      .tonoDePielPredominante()
      .then((data: any) => {
        this.tonoDePiel = data.average;
        this.loading = false;
      })
      .catch(err => {
        this.loading = false;
      });
  }
}
