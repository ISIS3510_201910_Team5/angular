import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TonoPielPromedioComponent } from './tono-piel-promedio.component';

describe('TonoPielPromedioComponent', () => {
  let component: TonoPielPromedioComponent;
  let fixture: ComponentFixture<TonoPielPromedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TonoPielPromedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TonoPielPromedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
