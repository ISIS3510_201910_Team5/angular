import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private basePath = 'https://us-central1-leskan-app.cloudfunctions.net/';

  constructor(private http: HttpClient) {}

  consultas(nombre, startDate, endDate) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.basePath +
            nombre +
            '?fechaInicio=' +
            startDate +
            '&fechaFin=' +
            endDate
        )
        .subscribe(
          (data: any) => {
            resolve(data.numeroDeConsultas);
          },
          err => {
            console.log(err);
            return reject(err.error.error);
          }
        );
    });
  }

  seguimientos(nombre, startDate, endDate) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.basePath +
            nombre +
            '?fechaInicio=' +
            startDate +
            '&fechaFin=' +
            endDate
        )
        .subscribe(
          (data: any) => {
            resolve(data.numeroDeSeguimientos);
          },
          err => {
            console.log(err);
            return reject(err.error.error);
          }
        );
    });
  }

  cambioColorPromedio(nombre, startDate, endDate) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.basePath +
            nombre +
            '?fechaInicio=' +
            startDate +
            '&fechaFin=' +
            endDate
        )
        .subscribe(
          (data: any) => {
            resolve(data.promedioCambio.toFixed(3));
          },
          err => {
            console.log(err);
            return reject(err.error.error);
          }
        );
    });
  }

  cambioTamanioPromedio(nombre, startDate, endDate) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.basePath +
            nombre +
            '?fechaInicio=' +
            startDate +
            '&fechaFin=' +
            endDate
        )
        .subscribe(
          (data: any) => {
            resolve(data.promedioCambio.toFixed(3));
          },
          err => {
            console.log(err);
            return reject(err.error.error);
          }
        );
    });
  }

  tiempoPromedioConsultas(nombre, startDate, endDate) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.basePath +
            nombre +
            '?fechaInicio=' +
            startDate +
            '&fechaFin=' +
            endDate
        )
        .subscribe(
          (data: any) => {
            resolve(
              Number((data.promedioDeTiempoPorConsultas / 1000).toFixed(2))
            );
          },
          err => {
            console.log(err);
            return reject(err.error.error);
          }
        );
    });
  }

  tiempoPromedioPorGenero(startDate, endDate) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.basePath +
            'tiempoDeAnalisisPromedioPorGenero' +
            '?fechaInicio=' +
            startDate +
            '&fechaFin=' +
            endDate
        )
        .subscribe(
          (data: any) => {
            resolve(data);
          },
          err => {
            console.log(err);
            return reject(err.error.error);
          }
        );
    });
  }

  consultasPromedioPorUsuario(startDate, endDate) {
    return new Promise((resolve, reject) => {
      this.http
        .get(
          this.basePath +
            'promedioDeConsultasPorUsuario?fechaInicio=' +
            startDate +
            '&fechaFin=' +
            endDate
        )
        .subscribe(
          (data: any) => {
            resolve(data.promedioDeConsultas);
          },
          err => {
            console.log(err);
            return reject(err.error.error);
          }
        );
    });
  }

  sexoPredominante() {
    return new Promise((resolve, reject) => {
      this.http.get(this.basePath + 'sexoPredominante').subscribe(
        (data: any) => {
          resolve(data);
        },
        err => {
          console.log(err);
          return reject(err.error.error);
        }
      );
    });
  }

  usuarios() {
    return new Promise((resolve, reject) => {
      this.http.get(this.basePath + 'numeroDeUsuarios').subscribe(
        (data: any) => {
          console.log(data);
          resolve(data.usuarios);
        },
        err => {
          console.log(err);
          return reject(err.error.error);
        }
      );
    });
  }

  usuariosSinConsultas() {
    return new Promise((resolve, reject) => {
      this.http.get(this.basePath + 'numeroDeUsuariosSinConsultas').subscribe(
        (data: any) => {
          resolve(data.numeroDeConsultas);
        },
        err => {
          console.log(err);
          return reject(err.error.error);
        }
      );
    });
  }

  rangoDeEdad() {
    return new Promise((resolve, reject) => {
      this.http.get(this.basePath + 'rangoDeEdad').subscribe(
        (data: any) => {
          resolve(data);
        },
        err => {
          console.log(err);
          return reject(err.error.error);
        }
      );
    });
  }

  tonoDePielPredominante() {
    return new Promise((resolve, reject) => {
      this.http.get(this.basePath + 'tonoDePielPredominante').subscribe(
        (data: any) => {
          resolve(data);
        },
        err => {
          console.log(err);
          return reject(err.error.error);
        }
      );
    });
  }
}
